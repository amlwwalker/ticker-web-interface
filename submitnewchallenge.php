<?php

include 'header.php';

if (isset($_GET['delete'])) {
echo "deleted entry ". $_GET['delete']."<br>";

$database->delete("challenges", [
"uid" => $_GET['delete']
]);

}
if (isset($_GET['challenge']) && isset($_GET['timelimit']) && isset($_GET['level'])) {
$challenge = $_GET['challenge'];
$timelimit = $_GET['timelimit'];
$genre = $_GET['genre'];
$level = $_GET['level'];

$database->insert("challenges", [
"challenge" => $challenge,
"level" => $level,
"timelimit" => $timelimit,
"genre" => $genre
]);
echo "entry added: ".$challenge." at level ".$level."<br>";
}
?>
<?php

include "footer.php"
?>
